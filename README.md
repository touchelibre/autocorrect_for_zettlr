# GASZ: GENERATOR of AUTOCORRECT Setting for ZETTLR software

## Description

Autocorrection is a function of Zettlr (but that exist too in other software) to replace a chain of string to an unicode character or an other chain of string.

In Zettlr, you can find the autocorrect setting in “file/preferences/preferences/autocorrect”. Currently, the ergonomy of zettlr is OK if you want just some autocorrection. But it is quite boring if you want a lot of autocorrection. 

So, I made GASZ. It is just a spreadsheet file to get a piece of code to modify directly the file “config.json”.

A big list of autocorrections (2678) is available by default:

- 462 mathematics and technical characters
- 233 emoji and picture characters with alternative shortcode (quicker)
- 315 phonetical characters (my convention or x-sampa convention)
- 1353 autocorrects with Github’s shortcode style
- 315 other characters

Today, you can add up to 3000 autocorrects. But if you want more, you can. Just copy / past the formula in cells is needed.


## How to use ?

1. Select your favorite unicode characters in “Define_your_AutoCorrect” tab. Put or not a “x” in “Selection” column.
2. Check if the input chain of string (“key” column) is OK for you. Else modify.
3. If you want others characters, go to “Unicode_Selection” tab for help, and go to the others preselected unicode table.
4.  Put the new unicode character and choice the key in “Define_your_AutoCorrect” tab.
5. Check there is no mistake, no redundancy and no empty cell. You can have several “key” for a same “value”. But you must have only one “value” for the same “key”.
6. Copy / past your result of setting from “Resut” tab to the file “config.json” like explained.
7. Run Zettlr and enjoy !!!

**Remarks :** We can return in default configuration, if you select only the autocorrects under “by default in zettlr” in “type of character” column. But for more security save your original config.json in other safe directory.


## Where is the file “config.json” ?

- **in Window:** `C:\Users\<your username>\AppData\Roaming\Zettlr`
- **in MacOS:** `/Users/<your username>/Library/Application Support/Zettlr`
- **in GNU/Linux:** `/home/<your username>/.config/Zettlr` (note that `.config` is a hidden folder)
- **in Flatpack:** `/home/<your username>/.var/app/com.zettlr.Zettlr/config/Zettlr`


## About Zettlr

See the [Zettlr web site](https://zettlr.com/).

Zettlr is in my opinion the best markdown editor for manage your note-taking. Very similar to Obsidian, but better because free like freedom and open source like GNU GPL.


## Font

Be carefull, universal font don’t exist. Any font can take in account all the unicode caracters. Please, check your favorite font can take in account your unicode target.

This file is writen with [Symbola font](https://dn-works.com/wp-content/uploads/2021/UFAS121921/Symbola.pdf ).

Symbola (the best for me) take in account almost all the latin, greek and cyrrilic caracters and almost all the “special” characters (mathematic, technical, emoji, icon, arrow, phonetic character…) !!!

The style of Symbola is academic, very similar to the LaTeX’s font by default. Symbola is with sherif and the icons and emojis are in black and white (no colour like permited by unicode). Symbola is not monospace, so you need an others font for code edition. Fira Code or inconsolata could be a good choice.

In Zettlr, you can change the font like explained here: [zettlr documentation about CSS](https://docs.zettlr.com/en/core/custom-css/). You can select Symbola for normal text and a monospace font for coding part.


## Disclaimer

GASZ is not a software, it is just hack. **There is no input control and no output control.** So mistake happens. **It is in your responsability to check the result.** The output piece of code shall be OK with json syntaxe and with zettlr features.

The preselection of unicode characters is focused on special symbols and icons. If you want write in other scrypt than latin scrypt, it is a better idea to install an other keyboard configuration in your OS. And so on, you should change all the “key” inputs in relation to your keyboard.

The autocorrection of LibreOffice could be in interference with your key definition. So please disable the LibreOffice’s autocorrection here: Tools/Autocorrection_Option/Option, then uncheck “Use the table of replacement”. Check or uncheck other linguistical options in relation to your language.
